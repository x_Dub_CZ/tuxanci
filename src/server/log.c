#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <assert.h>

#include "log.h"
#include "main.h"

static FILE *logFile;

int log_init(char *name)
{
	logFile = fopen(name, "a");

	if (logFile == NULL) {
		error("Unable to open log file [%s]", name);
		return -1;
	}

	debug("Using log file [%s]", name);

	log_add(LOG_INF, "Logging started");

	return 0;
}

void log_add(int type, char *msg)
{
	char str[STR_LOG_SIZE];
	struct tm *tm_struct;
	time_t currentTime;
	char *str_type;

	if (logFile == NULL) {
		return;
	}

	currentTime = time(NULL);
	tm_struct = localtime(&currentTime);

	switch (type) {
	case LOG_INF:
		str_type = "INFO";
		break;
	case LOG_DBG:
		str_type = "DEBUG";
		break;
	case LOG_WRN:
		str_type = "WARN";
		break;
	case LOG_ERR:
		str_type = "ERROR";
		break;
	default:
		fatal("Unknown type of the logging string");
		break;
	}

	sprintf(str, "[%02d-%02d-%02d %02d:%02d:%02d] [%s] %s\n",
			1900 + tm_struct->tm_year, tm_struct->tm_mon + 1, tm_struct->tm_mday,
			tm_struct->tm_hour, tm_struct->tm_min, tm_struct->tm_sec,
			str_type, msg);

	fprintf(logFile, "%s", str);

	fflush(logFile);
}

void log_quit()
{
	if (logFile == NULL) {
		return;
	}

	log_add(LOG_INF, "Logging finished");
	fclose(logFile);
}
