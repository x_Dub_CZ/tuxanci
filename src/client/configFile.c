#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "main.h"
#include "configFile.h"

int isYesOrNO(char *s)
{
	if (s[0] == 'Y' || s[0] == 'y') {
		return 1;
	} else {
		return 0;
	}
}

char *getYesOrNo(int n)
{
	return (n != 0 ? "YES" : "NO");
}

int getValue(char *line, char *env, char *val, int len)
{
	char *offset_env;
	char *offset_val_begin;
	char *offset_val_end;
	char clone_env[STR_SIZE];
	int val_len;

	strcpy(clone_env, env);

	offset_env = strstr(line, clone_env);

	if (offset_env == NULL) {
		return -1;
	}

	offset_val_begin = strchr(offset_env, '"');

	if (offset_val_begin == NULL) {
		return -1;
	}

	offset_val_end = strchr(offset_val_begin + 1, '"');

	if (offset_val_end == NULL) {
		return -1;
	}

	val_len = (int) (offset_val_end - (offset_val_begin + 1));

	if (val_len > len - 1) {
		val_len = len - 1;
	}

	memset(val, 0, len);
	memcpy(val, offset_val_begin + 1, val_len);

	return 0;
}

char *setValue(char *line, char *env, char *val)
{
	char *offset_env;
	char *offset_val_begin;
	char *offset_val_end;
	char clone_env[STR_SIZE];
	char clone[STR_SIZE];

	/*strcpy(clone_env, " ");*/
	strcpy(clone_env, env);

	offset_env = strstr(line, clone_env);

	if (offset_env == NULL) {
		return NULL;
	}

	offset_val_begin = strchr(offset_env, '"');

	if (offset_val_begin == NULL) {
		return NULL;
	}

	offset_val_end = strchr(offset_val_begin + 1, '"');

	if (offset_val_end == NULL) {
		return NULL;
	}

	memset(clone, 0, STR_SIZE);
	strncpy(clone, line, (int) ((offset_val_begin - line) + 1));
	strcat(clone, val);
	strcat(clone, offset_val_end);

	return strdup(clone);
}

int getValueInConfigFile(textFile_t * textFile, char *env, char *val, int len)
{
	int i;
	char *line;

	for (i = 0; i < textFile->text->count; i++) {
		line = (char *) (textFile->text->list[i]);

		if (strncmp(line, env, strlen(env)) == 0) {
			return getValue(line, env, val, len);
		}
	}

	return -1;
}

int setValueInConfigFile(textFile_t * textFile, char *env, char *val)
{
	int i;
	char *line;
	char *ret;

	for (i = 0; i < textFile->text->count; i++) {
		line = (char *) (textFile->text->list[i]);

		if (strncmp(line, env, strlen(env)) == 0) {
			ret = setValue(line, env, val);

			if (ret != NULL) {
				list_del_item(textFile->text, i, free);
				list_ins(textFile->text, i, ret);

				return 0;
			}
		}
	}

	return -1;
}

void loadValueFromConfigFile(textFile_t * textFile, char *env, char *val, int len, char *butVal)
{
	strcpy(val, butVal);

	if (getValueInConfigFile(textFile, env, val, len) != 0) {
		char line[STR_SIZE];

		sprintf(line, "%s=\"%s\"", env, butVal);
		list_add(textFile->text, strdup(line));

		debug("Expanding config file [%s]", line);
	}
}
