#ifndef ARCHIVE_H
#define ARCHIVE_H

extern char *archive_extract_file(char *archive, char *file);
extern void archive_delete_file(char *s);

#endif /* ARCHIVE_H */
