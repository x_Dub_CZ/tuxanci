#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "main.h"
#include "list.h"

typedef struct struct_storage_item {
	char *name;
	char *group;
	void *data;
} storage_item_t;

list_t *storage_new()
{
	return list_new();
}

static storage_item_t *storage_new_item(char *group, char *name, void *data)
{
	storage_item_t *new;

	assert(name != NULL);
	assert(group != NULL);
	assert(data != NULL);
	new = malloc(sizeof(storage_item_t));
	new->name = strdup(name);
	new->group = strdup(group);
	new->data = data;

	return new;
}

static void storage_destroy_item(storage_item_t *p, void *f)
{
	void (*fce) (void *);

	assert(p != NULL);
	assert(f != NULL);

	fce = f;
	fce(p->data);
	free(p->name);
	free(p->group);
	free(p);
}

void storage_add(list_t *list, char *group, char *name, void *data)
{
	assert(list != NULL);
	assert(group != NULL);
	assert(name != NULL);

	list_add(list, storage_new_item(group, name, data));
}

void *storage_get(list_t *list, char *group, char *name)
{
	storage_item_t *this;
	int i;

	assert(list != NULL);
	assert(group != NULL);
	assert(name != NULL);

	for (i = 0; i < list->count; i++) {
		this = (storage_item_t *) list->list[i];

		if (strcmp(group, this->group) == 0 && strcmp(name, this->name) == 0) {
			return this->data;
		}
	}

	debug("Storage item not found [%s %s]", group, name);

	return NULL;
}

void storage_del(list_t *list, char *group, char *name, void *f)
{
	storage_item_t *this;
	int i;

	assert(list != NULL);
	assert(group != NULL);
	assert(name != NULL);

	for (i = 0; i < list->count; i++) {
		this = (storage_item_t *) list->list[i];

		if (strcmp(group, this->group) == 0 && strcmp(name, this->name) == 0) {
			storage_destroy_item(this, f);
			list_del(list, i);
			return;
		}
	}

	return;
}

void storage_del_all(list_t *list, char *group, void *f)
{
	storage_item_t *this;
	int i;

	assert(list != NULL);
	assert(group != NULL);

	for (i = 0; i < list->count; i++) {
		this = (storage_item_t *) list->list[i];

		if (strcmp(group, this->group) == 0) {
			storage_destroy_item(this, f);
			list_del(list, i);
			i--;
		}
	}
}

void storage_destroy(list_t *p, void *f)
{
	storage_item_t *this;
	int i;

	assert(p != NULL);
	assert(f != NULL);

	for (i = 0; i < p->count; i++) {
		this = (storage_item_t *) p->list[i];

		storage_destroy_item(this, f);
	}

	list_destroy(p);
}
