#ifndef BOOL_H
#define BOOL_H

#include "main.h"

#define bool_t	int
#define TRUE	1
#define FALSE	0

#endif /* BOOL_H */
