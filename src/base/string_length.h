#ifndef STR_LENGTH_H
#define STR_LENGTH_H

#include "main.h"

#define STR_SIZE		128
#define STR_PROTO_SIZE		128
#define STR_LOG_SIZE		256
#define STR_NUM_SIZE		8
#define STR_NAME_SIZE		32
#define STR_PATH_SIZE		4096
#define STR_FILE_NAME_SIZE	256
#define STR_IP_SIZE		64

#endif /* STR_LENGTH_H */
