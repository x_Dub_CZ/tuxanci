#ifndef HOMEDIRECTOR_H
#define HOMEDIRECTOR_H

#include "main.h"

#define HOMEDIRECTOR_NAME ".tuxanci"

extern void home_director_create();
extern char *home_director_get();

#endif /* HOMEDIRECTOR_H */
