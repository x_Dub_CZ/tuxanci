#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "main.h"
#include "list.h"
#include "textFile.h"
#include "arenaFile.h"
#include "arena.h"
#include "tux.h"
#include "shot.h"
#include "item.h"
#include "director.h"
#include "modules.h"
#include "archive.h"
#include "homeDirector.h"

#ifndef PUBLIC_SERVER
#include "configFile.h"
#include "image.h"
#endif /* PUBLIC_SERVER */

#ifndef NO_SOUND
#include "music.h"
#endif /* NO_SOUND */

static list_t *listArenaFile;
static bool_t isArenaFileInit = FALSE;

bool_t arena_file_is_inicialized()
{
	return isArenaFileInit;
}

int arena_file_get_value(char *line, char *env, char *val, int len)
{
	char *offset_env;
	char *offset_val_begin;
	char *offset_val_end;
	char clone_env[STR_SIZE];
	int val_len;

	strcpy(clone_env, " ");
	strcat(clone_env, env);

	offset_env = strstr(line, clone_env);

	if (offset_env == NULL) {
		return -1;
	}

	offset_val_begin = strchr(offset_env, '"');

	if (offset_val_begin == NULL) {
		return -1;
	}

	offset_val_end = strchr(offset_val_begin + 1, '"');

	if (offset_val_end == NULL) {
		return -1;
	}

	val_len = (int) (offset_val_end - (offset_val_begin + 1));

	if (val_len > len - 1) {
		val_len = len - 1;
	}

	memset(val, 0, len);
	memcpy(val, offset_val_begin + 1, val_len);

	return 0;
}


#ifndef PUBLIC_SERVER
image_t *arena_file_load_image(arenaFile_t *arenaFile, char *filename, char *group, char *name, int alpha)
{
	char *extractFile;
	image_t *image;

	extractFile = archive_extract_file(arenaFile->path, filename);
	image = image_add(extractFile, alpha, name, group);
	archive_delete_file(extractFile);

	return image;
}

#ifndef NO_SOUND
void loadMusicFromArena(arenaFile_t *arenaFile, char *filename, char *group, char *name)
{
	char *extractFile;

	extractFile = archive_extract_file(arenaFile->path, filename);
	music_add(extractFile, name, group);
	archive_delete_file(extractFile);
}
#endif /* NO_SOUND */
#endif /* PUBLIC_SERVER */

static void cmd_arena(arena_t **arena, char *line)
{
	char str_image[STR_SIZE];
	char str_w[STR_SIZE];
	char str_h[STR_SIZE];

	if (arena_file_get_value(line, "background", str_image, STR_SIZE) != 0) {
		return;
	}

	if (arena_file_get_value(line, "w", str_w, STR_SIZE) != 0) {
		return;
	}

	if (arena_file_get_value(line, "h", str_h, STR_SIZE) != 0) {
		return;
	}

	(*arena) = arena_new(atoi(str_w), atoi(str_h));
#ifndef PUBLIC_SERVER
	(*arena)->background = image_get(IMAGE_GROUP_USER, str_image);
#endif /* PUBLIC_SERVER */
	/*
	(*arena)->w = atoi(str_w);
	(*arena)->h = atoi(str_h);
	*/
	arena_set_current(*arena);

	debug("Arena loaded");
}

static void cmd_module_load(char *line)
{
	char str_file[STR_SIZE];

	if (arena_file_get_value(line, "file", str_file, STR_SIZE) != 0) {
		return;
	}

	module_load(str_file);
}

#ifndef PUBLIC_SERVER
static void cmd_loadImage(arenaFile_t *arenaFile, char *line)
{
	char str_file[STR_SIZE];
	char str_name[STR_SIZE];
	char str_alpha[STR_SIZE];

	if (arena_file_get_value(line, "file", str_file, STR_SIZE) != 0) {
		return;
	}

	if (arena_file_get_value(line, "name", str_name, STR_SIZE) != 0) {
		return;
	}

	if (arena_file_get_value(line, "alpha", str_alpha, STR_SIZE) != 0) {
		return;
	}

	/*printf("str_file=%s str_name=%s str_alpha=%s\n", str_file, str_name, str_alpha);*/
	arena_file_load_image(arenaFile, str_file, IMAGE_GROUP_USER, str_name, isYesOrNO(str_alpha));

	/*image_add(str_file, isYesOrNO(str_alpha), str_name, IMAGE_GROUP_USER);*/
}

static void cmd_loadMusic(arenaFile_t *arenaFile, char *line)
{
	char str_file[STR_SIZE];
	char str_name[STR_SIZE];

	if (arena_file_get_value(line, "file", str_file, STR_SIZE) != 0) {
		return;
	}

	if (arena_file_get_value(line, "name", str_name, STR_SIZE) != 0) {
		return;
	}

#ifndef NO_SOUND
	/*music_add(str_file, str_name, MUSIC_GROUP_USER);*/
	loadMusicFromArena(arenaFile, str_file, MUSIC_GROUP_USER, str_name);
#endif /* NO_SOUND */
}

static void cmd_music_play(arena_t *arena, char *line)
{
	char str_music[STR_SIZE];

	if (arena_file_get_value(line, "music", str_music, STR_SIZE) != 0) {
		return;
	}

	strcpy(arena->music, str_music);
}
#endif /* PUBLIC_SERVER */

int arena_file_get_count()
{
	return listArenaFile->count;
}

static char *arena_file_get_status(textFile_t *ts, char *s)
{
	int len;
	int i;

	len = strlen(s);

	for (i = 0; i < ts->text->count; i++) {
		char *line;

		line = (char *) (ts->text->list[i]);

		if (strncmp(line, s, len) == 0) {
			return line + len + 1;
		}
	}

	return NULL;
}

char *arena_file_get_name(arenaFile_t *arenaFile)
{
	char *ret;

	ret = arena_file_get_status(arenaFile->map, "name");
	return (ret != NULL ? ret : "arena_no_name");
}

char *arena_file_get_net_name(arenaFile_t *arenaFile)
{
	char *ret;

	ret = arena_file_get_status(arenaFile->map, "netName");
	return (ret != NULL ? ret : "arena_no_net_name");
}

arenaFile_t *arena_file_get_file_format_net_name(char *s)
{
	int i;

	for (i = 0; i < listArenaFile->count; i++) {
		arenaFile_t *arenaFile;

		arenaFile = (arenaFile_t *) listArenaFile->list[i];

		if (strcmp(arena_file_get_net_name(arenaFile), s) == 0) {
			return arenaFile;
		}
	}

	return NULL;
}

char *arena_file_get_image(arenaFile_t *arenaFile)
{
	return arena_file_get_status(arenaFile->map, "screen");
}

arenaFile_t *arena_file_get(int n)
{
	return (arenaFile_t *) listArenaFile->list[n];
}

int arena_file_get_id(arenaFile_t *arenaFile)
{
	int i;

	for (i = 0; i < listArenaFile->count; i++) {
		arenaFile_t *this;

		this = (arenaFile_t *) listArenaFile->list[i];

		if (this == arenaFile) {
			return i;
		}
	}

	return -1;
}

arena_t *arena_file_get_arena(arenaFile_t *arenaFile)
{
	textFile_t *ts;
	arena_t *arena = NULL;		/* no warnings */
	int i;

	ts = (textFile_t *) arenaFile->map;

	for (i = 0; i < ts->text->count; i++) {
		char *line;

		line = (char *) ts->text->list[i];
		module_cmd(line);
#ifndef PUBLIC_SERVER
		if (strncmp(line, "loadImage", 9) == 0) {
			cmd_loadImage(arenaFile, line);
		}

		if (strncmp(line, "loadMusic", 9) == 0) {
			cmd_loadMusic(arenaFile, line);
		}

		if (strncmp(line, "playMusic", 9) == 0) {
			cmd_music_play(arena, line);
		}
#endif /* PUBLIC_SERVER */

		if (strncmp(line, "arena", 5) == 0) {
			cmd_arena(&arena, line);
		}

		if (strncmp(line, "loadModule", 10) == 0) {
			cmd_module_load(line);
		}
	}

	return arena;
}

arenaFile_t *arena_file_new(char *path)
{
	arenaFile_t *new;
	char *extractFile;

	new = malloc(sizeof(arenaFile_t));
	new->path = strdup(path);

	extractFile = archive_extract_file(path, "arena.map");
	new->map = text_file_load(extractFile);
	archive_delete_file(extractFile);

	return new;
}

void arena_destroyFile(arenaFile_t *p)
{
	text_file_destroy(p->map);
	free(p->path);
	free(p);
}

void arena_file_load(char *path)
{
	list_add(listArenaFile, arena_file_new(path));
}

static void load_arenaFromDirector(char *director)
{
	director_t *p;
	int i;

	debug("Loading arena files [%s]", director);
	p = director_load(director);

	if (p == NULL) {
		error("Directory not found [%s]", director);
		exit(-1);
	}

	unsigned l = strlen(director) - 1;

	for (i = 0; i < p->list->count; i++) {
		char *line;

		line = (char *) p->list->list[i];

		if (strstr(line, ".zip") != NULL && strstr(line, "~") == NULL) {
			char path[STR_PATH_SIZE];
			
			
			if (director[l] == (char) PATH_SEPARATOR[0]) {
				sprintf(path, "%s%s", director, line);
			} else {
				sprintf(path, "%s%s%s", director, PATH_SEPARATOR, line);
			}

			debug("Loading arena [%s]", line);

			accessExistFile(path);
			arena_file_load(path);
		}
	}

	/*printf("No. of Arens: %d\n", listArenaFile->count);*/
	director_destroy(p);

}

void arena_file_init()
{
#ifndef PUBLIC_SERVER
	assert(image_is_inicialized() == TRUE);
#endif /* PUBLIC_SERVER */
	isArenaFileInit = TRUE;
	listArenaFile = list_new();

	load_arenaFromDirector(PATH_ARENA);

#ifndef PUBLIC_SERVER
	load_arenaFromDirector(home_director_get());
#endif /* PUBLIC_SERVER */
}

void arena_file_quit()
{
	isArenaFileInit = FALSE;
	list_destroy_item(listArenaFile, arena_destroyFile);
}
