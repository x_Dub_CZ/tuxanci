#ifndef GUN_H
#define GUN_H

#define GUN_SHOT_WIDTH			5
#define GUN_SHOT_HEIGHT			5
#define GUN_LASSER_HORIZONTAL		40
#define GUN_SHOT_VERTICAL		5
#define GUN_BOMBBALL_WIDTH		20
#define GUN_BOMBBALL_HEIGHT		20

#include "main.h"
#include "tux.h"

extern void gun_shot(tux_t *p);

#endif /* GUN_H */
