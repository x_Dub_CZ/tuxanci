#ifndef DOWN_SERVER_H
#define DOWN_SERVER_H

#include "list.h"
#include "tcp.h"
#include "buffer.h"

extern int down_server_init(char *ip, int port);
extern int down_server_set_select();
extern int down_server_select_socket();
extern int down_server_quit();

#endif /* DOWN_SERVER_H */
