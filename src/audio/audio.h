#ifndef AUDIO_H
#define AUDIO_H

#include "main.h"

extern bool_t audio_is_inicialized();
extern void audio_init();
extern void audio_quit();

#endif /* AUDIO_H */
